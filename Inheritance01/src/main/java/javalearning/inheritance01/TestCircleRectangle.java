package javalearning.inheritance01;

public class TestCircleRectangle {

    public static void main(String[] args) {
        CircleFromSimpleGeometricObject circulito = new CircleFromSimpleGeometricObject(2);
        System.out.println("A circle " + circulito.toString());
        System.out.println("The color is " + circulito.getColor());
        System.out.println("The radius is " + circulito.getRadius());
        System.out.println("The area is " + circulito.getArea());
        System.out.println("The diameter is " + circulito.getDiameter());

        RectangleFromSimpleGeometricObject rectangulito = new RectangleFromSimpleGeometricObject(2, 4);
        System.out.println("\nA rectangle " + rectangulito.toString());
        System.out.println("The area is " + rectangulito.getArea());
        System.out.println("The perimeter is " + rectangulito.getPermiter());

    }
}
