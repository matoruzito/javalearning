import java.util.*;
public class TestCollection {
    public static void main(String[] argv){
        
        SetListPerformanceTest performance = new SetListPerformanceTest();
        performance.mainsito();
        
        ArrayList<String> coleccion1 = new ArrayList<>();
        coleccion1.add("Villa Los Coihues");
        coleccion1.add("Dina Huapi");
        coleccion1.add("Frutillar");
        
        System.out.println(coleccion1);
        
        ArrayList<String> coleccion2 = new ArrayList<>();
        
        
        Iterator<String> iterador = coleccion1.iterator();
        while(iterador.hasNext()){
            System.out.print(iterador.next().toUpperCase() + " ");
            
        }
        
        Iterator<String> iterador2 = coleccion1.iterator();
        
        while(iterador2.hasNext()){
            coleccion2.add(iterador2.next().toUpperCase());
        }
        
        
        System.out.println();
        
        System.out.println("La siguiente colección tiene todo en mayúscula: ");
        System.out.print(coleccion2);
        
        
        
        
    }
}
