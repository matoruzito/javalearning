
import java.util.*;

public class SetListPerformanceTest {

    //Remembering some terms:
    //    Static: It is the same for all instances
    //    Final: It is not overridable by sub-classes
    //    
    static final int N = 500000;
    
    public void SetListPerformanceTest(){
        //Empty constructor.
    }
   

    public void mainsito() {
        //Add numbers 0,1,2,3, ... , N-1 to the array list
        List<Integer> list = new ArrayList<Integer>();
        for (int i = 0; i < N; i++) {
            list.add(i);
            System.out.println(i);
        }
        Collections.shuffle(list); //Shuffle the array list using method inherited from Collections
        Collection<Integer> set1 = new HashSet<Integer>(list);
        System.out.println("Member test time for hash set is: " + getTestTime(set1) + " milliseconds");
        System.out.println("Remove element time for hash set is: " + getRemoveTime(set1) + " milliseconds");

        Collection<Integer> set2 = new LinkedHashSet<Integer>(list);
        System.out.println("Member test time for linked hash set is: " + getTestTime(set2) + " milliseconds");
        System.out.println("Remove element time for linked hash set is " + getRemoveTime(set2) + "milliseconds");

        Collection<Integer> set3 = new TreeSet<Integer>(list);
        System.out.println("Member test time for tree set is " + getTestTime(set3) + " milliseconds");
        System.out.println("Remove element time for tree set is " + getRemoveTime(set3) + " milliseconds");

        Collection<Integer> list1 = new ArrayList<Integer>(list);
        System.out.println("Member test time for arraylist is " + getTestTime(list1) + " milliseconds");
        System.out.println("Remove element time for arraylist is " + getRemoveTime(list1) + " milliseconds");

        Collection<Integer> list2 = new LinkedList<Integer>(list);
        System.out.println("Member test time for linked list is " + getTestTime(list2) + " milliseconds");
        System.out.println("Remove element time for linked list is " + getRemoveTime(list2) + " milliseconds");

    }

    public static long getTestTime(Collection<Integer> c) {
        long startTime = System.currentTimeMillis();
        for (int i = 0; i < N; i++) {
            c.contains((int) Math.random() * 2 * N);
        }
        return System.currentTimeMillis() - startTime;
    }

    public static long getRemoveTime(Collection<Integer> c) {
        long startTime = System.currentTimeMillis();
        for (int i = 0; i < N; i++) {
            c.remove(i);
        }
        return System.currentTimeMillis() - startTime;

    }

}
