package javalearning.oop2;

public class TestTV {
    public static void main(String[] Args){
        TV tv1 = new TV();  //I can instance TV objects as many times as I want.
        tv1.turnOn();
        tv1.setChannel(45);
        tv1.setVolume(5);
        
        TV tv2 = new TV();   //What did I just say? ^
        tv2.turnOn();
        tv2.setChannel(60);
        tv2.setVolume(3);
        
        System.out.println("tv1's channel is " + tv1.channel + " And volume level is " + tv1.volumeLevel);
        
               
    }
}
