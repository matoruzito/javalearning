package javalearning.filemanagement;

import java.io.FileNotFoundException;
import java.util.Scanner;

public class WriteData {

    public static void main(String[] args) throws FileNotFoundException {
        boolean ok = false;
        while (!ok) {
            System.out.print("Give me the file name: ");
            Scanner userOptions = new Scanner(System.in);
            String fileName = userOptions.nextLine();
            java.io.File archivo = new java.io.File(fileName); //Making an instance of File class. 
            if (!archivo.exists()) {
                java.io.PrintWriter salida = new java.io.PrintWriter(archivo);  //Instance of PrintWriter class
                salida.print("Esto se escribe en el archivo");
                salida.println("\t Esto ocasionará un salto de línea");

                String datosEnVariables = "Estos son datos en variables!";

                salida.print(datosEnVariables);

                salida.close(); //Clossing file.

                
                //Create 10000 files :) jeje
                for(int i = 0; i < 10000; i++){
                    java.io.File arc = new java.io.File("name-"+i); //Making an instance of File class. 
                    java.io.PrintWriter output = new java.io.PrintWriter(arc);
                    output.println("This file is numbered: " + i);
                    output.close();
                }
                
                
                ok = true; //Stop program

            } else {
                System.out.println("That file already exists.");
                ok = false;      //Ask another time for the fileName.
            }
        }

    }
}
