package javalearning.interfaces01;

public interface Edible {

    public abstract String howToEat();
}
