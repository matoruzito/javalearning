package javalearning.binaryio;

import java.io.*;

public class TestFileStream {

    public static void main(String[] args) throws IOException {
        EcrireDansDatFile archiver = new EcrireDansDatFile();
        
        archiver.ecrireData();
        
        System.out.println("L'archive est completèment ecrit.");
        
        LireDataDeFile lectorador = new LireDataDeFile(); 
        
        lectorador.lireData();
        
        
    }
}
