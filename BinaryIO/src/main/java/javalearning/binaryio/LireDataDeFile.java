package javalearning.binaryio;

import java.io.FileInputStream;
import java.io.IOException;

public class LireDataDeFile {

    public void LireDataDeFile() {
        //Seulèment le contructor

    }

    //Sous cet line, il sonts les methodes.
    public void lireData() throws IOException {
        FileInputStream entrance = new FileInputStream("depart.dat");

        int value;
        //Cet condition est pour connaitre si la line n'est pas finalicé
        while ((value = entrance.read()) != -1) {
            System.out.print(value + " ");

        }
        entrance.close();
    }

}
