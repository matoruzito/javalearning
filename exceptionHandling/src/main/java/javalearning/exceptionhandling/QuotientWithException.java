package javalearning.exceptionhandling;

import java.util.Scanner;

public class QuotientWithException {

    public static int quotient(int n1, int n2) {
        if (n2 == 0) {
            throw new ArithmeticException("Division by zero not allowed");
        }
        return n1 / n2;
    }

    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        System.out.print("Give me first number: ");
        int n1 = input.nextInt();
        System.out.print("Give me second number: ");
        int n2 = input.nextInt();
        try {
            int result = quotient(n1, n2);
            System.out.println(n1 + " / " + n2 + " = " + result);
        } catch (ArithmeticException ex) {
            System.out.println(ex);
        }
        System.out.println("All continues, nothing broken");

    }

}
