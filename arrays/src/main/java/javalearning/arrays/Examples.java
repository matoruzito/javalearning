package javalearning.arrays;

public class Examples {

    public static void exampleUnidimensional() {

        String[] nombres;
        nombres = new String[5]; //Nombres is an array of strings of ten positions.
        nombres[0] = "Nataniel";
        nombres[1] = "Camila";
        nombres[2] = "Jose";
        nombres[3] = "Nicolas";
        nombres[4] = "Sarah";

        //Lets print the names:
        for (int i = 0; i < 5; i++) {
            System.out.println("[" + i + "]" + nombres[i]);
        }

    }
    
    public static void exampleMultidimensional(){
        String[][] informacionDelPersonal = new String[2][2];
        informacionDelPersonal[0][0] = "Nataniel";       //Name
        informacionDelPersonal[0][1] = "15";             //Age
        informacionDelPersonal[1][0] = "Camila";       
        informacionDelPersonal[1][1] = "20";             
         
        
        for(int i = 0; i < 2; i++){
            for(int j = 0; j < 2; j++){
                    System.out.print(informacionDelPersonal[i][j] + " ");
                }
                System.out.println();
            }
        }
        
        
        
    
    }
    
  