package javalearning.arrays;

public class main {

    public static void main(String[] Args) {
        System.out.println("Unidimensional Example:");
        Examples.exampleUnidimensional();
        System.out.println("Multidimensional Example:");
        Examples.exampleMultidimensional();
    
    }   
    
}
