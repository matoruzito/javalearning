package javalearning.oop01;

public class Circle {

    public static void main(String[] Args) {
        System.out.println("Let's create a circle!");

        //To call a constructor let do this:
        ObjetosCirculo miPrimerCirculito = new ObjetosCirculo();

        System.out.println(""
                + "The area of the circle of radius "
                + miPrimerCirculito.radius
                + " is "
                + miPrimerCirculito.getArea()
        );
        
        

    }

}

class ObjetosCirculo {

    double radius;

    /* Construct a circle object */
    ObjetosCirculo() {
        radius = 1;
    }

    ObjetosCirculo(double newRadius) {
        radius = newRadius;
    }

    /*Return the area of this circle*/
    double getArea() {
        return radius * radius * Math.PI;

    }

    void setRadius(double newRadius) {
        radius = newRadius;
    }

}
