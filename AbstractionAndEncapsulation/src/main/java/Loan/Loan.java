
package Loan;
import java.util.Scanner;
import java.util.Date;




public class Loan {
    private double annualInterestTasa;
    private int numberDeYears;
    private double loanCantidad;
    private java.util.Date loanFecha;
    
    public Loan(){
        this(2.5,1,1000);
    }
    
    public Loan(double annualInterestRate, int numberOfYears, double loanAmount){
        this.annualInterestTasa = annualInterestRate;
        this.numberDeYears = numberOfYears;
        this.loanCantidad = loanAmount;
        loanFecha = new java.util.Date();
    }
    public double getAnnualInterestRate(){
        return annualInterestTasa;
    }
    public void setAnnualInterestTasa(double annualInterestTasa){
        this.annualInterestTasa = annualInterestTasa;
    }
    public int getNumberDeYears(){
        return numberDeYears;
    }
    public void setNumberOfYears(int numberDeYears){
        this.numberDeYears = numberDeYears;
    }
    public double getLoanCantidad(){
        return loanCantidad;
    }
    public void setLoanCantidad(double loanCantidad){
        this.loanCantidad = loanCantidad;
    }
    public double getMonthlyPayment(){
        double monthlyInterestTasa = annualInterestTasa / 1200;
        double monthlyPayment = loanCantidad * monthlyInterestTasa / ( 1 - (1 / Math.pow(1 + monthlyInterestTasa, numberDeYears * 12)));
        return monthlyPayment;
                
    }
    public double getTotalPayment(){
        double totalPayment = getMonthlyPayment() * numberDeYears * 12;
        return totalPayment;
    }
    public java.util.Date getLoanDate(){
        return loanFecha;
    }
         
 
}
