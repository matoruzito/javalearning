package javalearning.copier;

import java.io.*;
import java.util.Scanner;

public class Copy {

    /*
        Main Method
        @param argv[0] for source file
        @param argv[1] for destination file
     */
    public static void main(String[] argv) throws IOException {
         Scanner option = new Scanner(System.in);
         System.out.print("Give me the first filename: ");
         String from = option.nextLine(); 
         System.out.print("Give me the destination filename: ");
         String to = option.nextLine();
         String[] pasamientoDeArgumento = {from, to};
         
         copiar(pasamientoDeArgumento);
            
    }

    private static void copiar(String[] argument) throws IOException{
        if (argument.length != 2) {
            System.out.println("Usage: Java Copy sourceFile targetFile");
            System.exit(1);
        }

        File sourceFile = new File(argument[0]);
        if (!sourceFile.exists()) {
            System.out.println("Source file " + argument[0] + " does not exist");
            System.exit(2);
        }

        File targetFile = new File(argument[1]);
        if (targetFile.exists()) {
            System.out.println("Target file " + argument[1] + " already exists ");
            System.exit(3);
        }

        BufferedInputStream input = new BufferedInputStream(new FileInputStream(sourceFile));
        BufferedOutputStream output = new BufferedOutputStream(new FileOutputStream(targetFile));

        int k, numberOfBytesCopied = 0;

        //Reads a byte from input, assigns to k, only if that byte is not the end of file(-1)
        while ((k = input.read()) != -1) {
            output.write((byte) k);
            numberOfBytesCopied++;
        }

        input.close();
        output.close();

        System.out.println(numberOfBytesCopied + " bytes copied.");

    }

}
