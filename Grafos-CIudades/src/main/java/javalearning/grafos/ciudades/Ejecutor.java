package javalearning.grafos.ciudades;

public class Ejecutor {

    public static Graph ejecutor() {  //Signature 

        //Definimos los nodos del grafo.
        Node bue = new Node("Buenos Aires");
        Node cor = new Node("Cordoba");
        Node san = new Node("San Juan");
        Node men = new Node("Mendoza");
        Node sal = new Node("Salta");
        Node tuc = new Node("Tucuman");
        Node igu = new Node("Iguazu");

        bue.addEdge(new Edge(bue, cor, 1442));
        bue.addEdge(new Edge(bue, san, 1901));
        bue.addEdge(new Edge(bue, men, 2449));
        bue.addEdge(new Edge(bue, sal, 2026));
        bue.addEdge(new Edge(bue, tuc, 2400));

        cor.addEdge(new Edge(cor, san, 2009));
        cor.addEdge(new Edge(cor, men, 1833));
        cor.addEdge(new Edge(cor, igu, 1435));

        sal.addEdge(new Edge(sal, igu, 1747));

        Graph graph = new Graph();

        graph.addNode(bue);
        graph.addNode(cor);
        graph.addNode(sal);
        graph.addNode(tuc);
        graph.addNode(san);
        graph.addNode(men);
        graph.addNode(igu);

        Node from = bue;
        Node to = igu;

        double costoMinimoCalculado = CalcMinCost.MinCost(from, to);

        if (costoMinimoCalculado == 0) {
            //No existe destino directo.
            //Llamamos a calcularMinCostDestinoIndirecto.
            System.out.println(CalcMinCost.MinCostIndirecto(from, to));
        } else {
            System.out.println("El costo minimo desde " + from.getCity()
                    + " hacia " + to.getCity()
                    + " es: " + costoMinimoCalculado);
        }

        return graph;
    }

    public static void main(String[] args) {
        Graph graph = ejecutor();

    }

}
