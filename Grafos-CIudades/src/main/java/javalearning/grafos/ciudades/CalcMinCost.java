package javalearning.grafos.ciudades;

import java.util.List;

public class CalcMinCost {

    public static double MinCost(Node desde, Node hacia) {
        //Caso N1 => Camino directo

        List<Edge> aristas = desde.getEdges();
        int i = 0;
        int cantAristas = aristas.size();
        double costoMin = 0;
        while (i < cantAristas) {
            Edge aristaTemporal = aristas.get(i);
            if (aristaTemporal.getDestination() == hacia) {
                /*  Para cada arista, vemos cual es el costo, 
                    y guardamos el menor. */
                double costo = aristaTemporal.getDistance();
                if ((costo < costoMin) || (costoMin == 0)) {
                    costoMin = costo;
                }
            }

            i++;
        }

        return costoMin;
    }

    public static double MinCostIndirecto(Node desde, Node hacia){
        Graph grafo = new Graph();
        List<Node> nodosDelGrafo = grafo.getNodes();
        List<Node> nodosConDestinoHacia = null;
        nodosConDestinoHacia.add(hacia);
        for (Node nodito : nodosDelGrafo) {
            for (Edge edgitos : nodito.getEdges()) {
                if (edgitos.getDestination() == hacia) {
                    nodosConDestinoHacia.add(nodito);
                }
            }
        }
        double costoMin = 0;
        for (Node n2 : nodosConDestinoHacia) {
            if (( MinCost(n2, hacia) + (MinCost(desde, n2))) < costoMin || costoMin == 0) {
                costoMin = MinCost(n2, hacia) + MinCost(desde, n2);
            }
        }

        return costoMin; 
    }
}
