package javalearning.strings;

public class ManageStrings {
    
    public static void main(String[] Args){
        
        String newString = new String("This is another message");
        String message = "This is another message";
       
        // To see if they have the same content
        
        if(newString.equals(message)){
            System.out.println("NewString and message are equal");
        }
        else{
            System.out.println("They are different");
        }
    
    
    }
    
    
    
}
