package javalearning.firstdbconection;

import java.sql.*;

public class DBConnection {

    private static final String DBADDR = "localhost";
    private static final String DBPORT = "3306";
    private static final String DBNAME = "javalearning";
    private static final String DBUSER = "root";
    private static final String DBPASS = "";

    public static Connection mariaDBConn() {
        Connection Conn;
        String urlCon;
        try {
            Class.forName("org.mariadb.jdbc.Driver");
            urlCon = "jdbc:mariadb://"
                    + DBADDR + ":"
                    + DBPORT + "/"
                    + DBNAME + "?user="
                    + DBUSER + "&password="
                    + DBPASS;

            Conn = DriverManager.getConnection(urlCon);

            System.out.println("Success login...");
            return Conn;
        } catch (Exception e) {
            System.out.println(e);
            return null;
        }
    }
}
