package javalearning.gettingdatafromweb;

import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.util.Scanner;

public class ReadDataFromURL {

    public static void main(String[] argv) throws FileNotFoundException {
        System.out.print("Give me a complete URL: ");
        String link = new Scanner(System.in).next();
        
        PrintWriter fileOutput = new PrintWriter("output1.txt"); //Instancing file to write the data collected from website there.
        
        
        try {
            java.net.URL url = new java.net.URL(link);  //Making an instance of URL object.
            int counter = 0;
            Scanner input = new Scanner(url.openStream());
            while (input.hasNext()) {
                String line = input.nextLine();
                fileOutput.println(line); //Writting inside output.txt the line that comes from website.
                System.out.println(line); //Also printing line to console.
                counter += line.length();
            }
            System.out.println("File size is: " + counter + " bytes");
            fileOutput.close();
        } catch (java.net.MalformedURLException ex) {
            System.out.println("This URL is not valid. ");
        } catch (java.io.IOException ex) {
            System.out.println("I/O errors!");
        }
    }
}
