package javalearning.multipleclasses1;

import java.util.Scanner;

/*  
    Modificator "Extends" allows me to use global variables defined into TransportWays without
    have to pass them as a parameter of every method.
 */
public class Airplanes extends TransportWays {

    private int branch;
    private String[] branchs = {"Boeing", "Airbus", "Cessna"};
    private int cantBranchsOptions = branchs.length;
    private int i = 0;

    public Airplanes() {
        System.out.println("Those are our airplanes: ");
        while (i < cantBranchsOptions) {
            System.out.println("[" + i + "] : " + branchs[i]);

            i++;
        }
        System.out.print("Select an airplane from the list: =>  ");
        Scanner input = new Scanner(System.in);
        branch = Integer.parseInt(input.nextLine());    //We save the option selected by the user
        
        
        if (forDevelopers) {
            System.out.println("New object of class Airplanes created. Branch: " + branchs[branch]);
        }
        
        
    }
    
    public String getBranch(){
        return branchs[branch];
    }

}
