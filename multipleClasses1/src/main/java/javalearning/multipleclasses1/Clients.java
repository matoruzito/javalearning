package javalearning.multipleclasses1;

import java.util.Scanner;

public class Clients extends TransportWays {

    private int age;
    private String name;

    //Constructor of the client
    public Clients() {
        System.out.print("What is your name, traveler? \t");
        Scanner input = new Scanner(System.in);
        name = input.nextLine();
        System.out.println("Hi " + name + "! Welcome to TransportWays Inc.");
        System.out.print("Please, tell us your age: \t");
        age = Integer.parseInt(input.nextLine());
        if (forDevelopers) {
            System.out.println("Initialized object type Clients with name: " + name + " and age: " + age);
        }

    }
    
    
    public String getName(){
        return name;
    }
    
    public int getAge(){
        return age;
    }
    
    
    
}

  