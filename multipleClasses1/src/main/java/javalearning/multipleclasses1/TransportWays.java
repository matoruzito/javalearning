package javalearning.multipleclasses1;

import java.util.Scanner;

public class TransportWays {

    /*Global variables*/
    public static Boolean forDevelopers = true;

    /*End global variables*/
    public static void main(String[] Args) {

        boolean continueRunning = true;
        int optionSelected;

        while (continueRunning) {
            Clients newClient = new Clients();      //Initializing the client by using the constructor.

            System.out.println("Our company has a lot of vehicles to travel around the world. Please, rent one of them: \n [1] : Car \n [2] : Airplane");
            Scanner input = new Scanner(System.in);
            optionSelected = Integer.parseInt(input.nextLine());

            switch (optionSelected) {
                case 1:
                    if (forDevelopers) {
                        System.out.println("You've choosen option 1 into Clients.menu().");
                    }
                    Cars newCar = new Cars();

                    System.out.println("-----------------------------------------");
                    System.out.println("Your travel information:");
                    System.out.println("-----------------------------------------");
                    System.out.println("Name: " + newClient.getName());
                    System.out.println("Age: " + newClient.getAge());
                    System.out.println("Car: " + newCar.getBranch());
                    System.out.println("Color: " + newCar.getColor());
                    System.out.println("-----------------------------------------");

                    break;
                case 2:
                    if (forDevelopers) {
                        System.out.println("You've choosen option 2 into Clients.menu().");

                    }
                    Airplanes newAirplane = new Airplanes();

                    System.out.println("-----------------------------------------");
                    System.out.println("Your travel information:");
                    System.out.println("-----------------------------------------");
                    System.out.println("Name: " + newClient.getName());
                    System.out.println("Age: " + newClient.getAge());
                    System.out.println("Airplane: " + newAirplane.getBranch());
                    System.out.println("-----------------------------------------");
                    
                    break;

                default:
                    if (forDevelopers) {
                        System.out.println("Errror.");
                    }

                    break;
            }

            continueRunning = false;    //Put this in true to loop the program
        }

    }

}
