package javalearning.multipleclasses1;

import java.util.Scanner;

/*  
    Modificator "Extends" allows me to use global variables defined into TransportWays without
    have to pass them as a parameter of every method.
 */
public class Cars extends TransportWays {

    private int color;
    private int branch;
    private String[] colors = {"Red", "Yellow", "Blue", "Grey", "Black"};
    private String[] branchs = {"Toyota", "Suzuki", "Mitsubishi", "Nissan", "Chevrolet", "Mazda", "Volkswagen", "Aston Martin", "Land Rover", "Jaguar",};

    private int cantColorsOptions = colors.length;
    private int cantBranchsOptions = branchs.length;

    private int i = 0;

    //Constructor of car
    public Cars() {
        System.out.println("This is our list of cars: ");
        //It shows the list of branchs
        while (i < cantBranchsOptions) {
            System.out.println("[" + i + "] : " + branchs[i]);

            i++;
        }
        System.out.print("Select a car from the list: => ");

        Scanner input = new Scanner(System.in);
        branch = Integer.parseInt(input.nextLine());

        System.out.print("Select the color of the car: ");

        i = 0;
        while (i < cantColorsOptions) {
            System.out.print("[" + i + "] : " + colors[i]);
            System.out.println();
            i++;
        }
        System.out.print("Select the color of the car: => ");

        color = Integer.parseInt(input.nextLine());

        if (forDevelopers) {
            System.out.println("New object of class Cars created. Branch: " + branchs[branch] + " & Color: " + colors[color]);
        }
    }

    public String getColor() {
        return colors[color];
    }

    public String getBranch() {
        return branchs[branch];
    }

}
