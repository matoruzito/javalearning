/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package javalearning.methods01;

/**
 *
 * @author matias
 */
public class Class01 {
    //Inside here I can write the methods asociated to this class

    public static void sayHi() {
        System.out.println("Hi!");
        sayNo();
    }

    private static void sayNo() {
        System.out.println("No");
    }

}
