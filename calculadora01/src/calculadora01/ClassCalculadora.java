    
package calculadora01;

import java.io.IOException;     //Para que permita el System.in.read()
import java.util.Scanner;




public class ClassCalculadora {
   
    public ClassCalculadora(){
        //Constructor -> Como pepe el constructor
        //No sé aún qué rol cumple el constructor aquí pero parece ser needle. 
        
    }   
  
    public double sumar(double p1, double p2){
        double resultado; 
        resultado = p1 + p2; 
        return resultado; 
    }
    public double restar(double p1, double p2){
        double resultado; 
        resultado = p1 - p2; 
        return resultado; 
    }
    public double multiplicar(double p1, double p2){
        double producto; 
        producto = p1 * p2; 
        return producto; 
    }
    public double dividir(double p1, double p2){
        //Verifiquemos que el divisor no sea 0, porque sino explota la cosa: 
        if(p2 == 0){
           String mensaje = "Como vas a querer dividir entre cero burro!";
           System.out.println(mensaje);
        }else{
            double cociente; 
            cociente = p1 / p2; 
            return cociente; 
            
        }
        return 0;        
        
    }
    
    
    public void debug1(){
        ClassCalculadora calculadoritaDebug = new ClassCalculadora(); 
        double testSuma = calculadoritaDebug.sumar(2,3);
        System.out.println("Suma 2 + 3: " + testSuma);
        double testResta = calculadoritaDebug.restar(2,1);
        System.out.println("Resta 2 -1: " + testResta);
        double testMultiplicacion = calculadoritaDebug.multiplicar(7,8);
        System.out.println("Multiplicacion 7x8: " + testMultiplicacion);
        double testDivision = calculadoritaDebug.dividir(14,7);
        System.out.println("Division 14/7: " + testDivision);
        double testDivisionErronea = calculadoritaDebug.dividir(14,0);
    }
    
    
    public void calcularSegunOpcionUsuario() throws IOException{
        while(true){
            ClassCalculadora calculadora = new ClassCalculadora(); 
            Scanner teclado = new Scanner(System.in);
            int opcion; 
            double primerNumero, segundoNumero; 
            String mensaje = "Elija: \n 1 -> Sumar \n 2 -> Restar \n 3 -> Multiplicar \n 4->Dividir \n \n 5->Salir \n";
            System.out.println(mensaje);
            opcion = teclado.nextInt();
            Double devolver; 

            switch (opcion) {
                case 1: 
                    //User has choosen Sumar
                    mensaje = "Ingrese primer numero: \t ";
                    System.out.println(mensaje);
                    primerNumero = teclado.nextDouble();
                    mensaje = "Ingrese segundo numero: \t ";
                    System.out.println(mensaje);
                    segundoNumero = teclado.nextDouble();
                    devolver = calculadora.sumar(primerNumero, segundoNumero);
                    System.out.println(devolver);
                break; 
                case 2: 
                    //User has choosen Restar
                    mensaje = "Ingrese primer numero: \t ";
                    System.out.println(mensaje);
                    primerNumero = teclado.nextDouble();
                    mensaje = "Ingrese segundo numero: \t ";
                    System.out.println(mensaje);
                    segundoNumero = teclado.nextDouble();
                    devolver = calculadora.restar(primerNumero, segundoNumero);
                    System.out.println(devolver);
                break; 
                case 3: 
                    //User has choosen Multiplicar
                    mensaje = "Ingrese primer numero: \t ";
                    System.out.println(mensaje);
                    primerNumero = teclado.nextDouble();
                    mensaje = "Ingrese segundo numero: \t ";
                    System.out.println(mensaje);
                    segundoNumero = teclado.nextDouble();
                    devolver = calculadora.multiplicar(primerNumero, segundoNumero);
                    System.out.println(devolver);
                break;
                case 4: 
                    //User has choosen Dividir
                    mensaje = "Ingrese primer numero: \t ";
                    System.out.println(mensaje);
                    primerNumero = teclado.nextDouble();
                    mensaje = "Ingrese segundo numero: \t ";
                    System.out.println(mensaje);
                    segundoNumero = teclado.nextDouble();
                    devolver = calculadora.dividir(primerNumero, segundoNumero);
                    System.out.println(devolver);
                break; 
                case 5: 
                    //User has choosen end
                    System.exit(0);
            }
        }
    }

}
