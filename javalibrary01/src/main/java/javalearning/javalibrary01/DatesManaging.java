package javalearning.javalibrary01;

public class DatesManaging {

    public static void main(String[] Args) {
        java.util.Date date = new java.util.Date();
        System.out.println(""
                + "The elapsed time since Jan 1, 1970 is "
                + date.getTime()
                + " milliseconds"
        );
        System.out.println(date.toString());
    }

}
