package javalearning.matrix2x2;

public class RandomMatrixCreator {

    private int[][] randomMatrix;
    private int i = 0, columnNumber = 2, rowsNumber = 2, cantNumbersTotal = 4;

    public RandomMatrixCreator() {
        //Empty constructor, creates a default matrix of 2x2 
        randomMatrix = new int[columnNumber][rowsNumber];
        /*
            a b 
            c d 
        
            randomMatrix[0][0] = a
            randomMatrix[0][1] = b
            randomMatrix[1][0] = c
            randomMatrix[1][1] = d
        
         */

    }

    //Constructor with int columns and rows number
    public RandomMatrixCreator(int co, int fi) {
        columnNumber = co;
        rowsNumber = fi;
        cantNumbersTotal = columnNumber * rowsNumber;

        randomMatrix = new int[columnNumber][rowsNumber];
        for (int k = 0; k < columnNumber; k++) {
            for (int j = 0; j < rowsNumber; j++) {
                randomMatrix[k][j] = i;
                i++;
            }
        }
    }

    //Getter for geting the matrix content
    public int[][] getContent() {
        return randomMatrix;
    }

    //Getter for printing matrix to console
    public void printMatrix() {
        for (int n = 0; n < columnNumber; n++) {
            System.out.print("|");
            for (int j = 0; j < rowsNumber; j++) {
                if (cantNumbersTotal <= 100) {
                    if (randomMatrix[n][j] < 10) {
                        System.out.print("0");
                    }
                }
                if (cantNumbersTotal > 100) {
                    if (randomMatrix[n][j] < 100) {
                        if (randomMatrix[n][j] < 10) {
                            System.out.print("00");
                        } else {
                            System.out.print("0");
                        }

                    }

                }
                System.out.print(randomMatrix[n][j] + "|");
            }
            System.out.println();

        }
    }

}
