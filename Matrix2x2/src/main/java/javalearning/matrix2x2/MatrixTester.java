package javalearning.matrix2x2;

import static java.lang.System.in;
import java.util.InputMismatchException;
import java.util.Scanner;

public class MatrixTester {

    static int a, b;
    static boolean exit;

    public static void main(String[] argv) {
        while (!exit) {
            ask();
            RandomMatrixCreator matrixita = new RandomMatrixCreator(a, b);
            matrixita.printMatrix();
            askForExit();
        }

    }

    private static void ask() {

        java.util.Scanner scan = new Scanner(in);
        do {
            System.out.print("Give me the rows number [1 - 30]: ");
            a = scan.nextInt();
            System.out.print("Give me the column number [1 - 30]: ");
            b = scan.nextInt();

            if (a > 30 || b > 30 || a == 0 || b == 0) {
                System.out.println("I'll ask you again, this time please respect the parameters.");
            }
        } while (a > 30 || b > 30 || a == 0 || b == 0);
        //Finally I found some really useful utilization for do-while :) 

    }

    private static void askForExit() {
        java.util.Scanner scan = new Scanner(in);
        System.out.println("To re-start choose 1, to exit choose 0: ");
        try {
            switch (scan.nextInt()) {
                case 0:
                    exit = true;
                    break;
                case 1:
                    exit = false;
                    break;
                default:
                    exit = false;
                    break;
            }
        } catch (InputMismatchException ex) {
            System.out.println("You should put an integer, sorry");
            
        }
    }

}
