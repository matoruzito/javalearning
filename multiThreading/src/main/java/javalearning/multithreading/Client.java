package javalearning.multithreading;

public class Client {

    public static void main(String[] argv) throws InterruptedException {
        TaskClass task1 = new TaskClass();
        Task2 task2 = new Task2();
        Task3 task3 = new Task3();

        Thread thread1 = new Thread(task1);
        Thread thread2 = new Thread(task2);
        Thread thread3 = new Thread(task3);

        thread1.setPriority(5);
        thread2.setPriority(10);
        thread3.setPriority(1);

        thread1.start();
        thread2.start();
        thread3.start();

        thread1.join();
        thread2.join();
        thread3.join();

        System.out.println("Task 1 => " + task1.getTimeSpent());
        System.out.println("Task 2 => " + task2.getTimeSpent());
        System.out.println("Task 3 => " + task3.getTimeSpent());

    }
}
