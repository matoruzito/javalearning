package javalearning.multithreading;

public class TaskClass implements Runnable {

    private long timeSpentTask1 = 0;
    private long timeNow = 0;
    private long timeSpentTotal = 0;

    public TaskClass() {
    }

    @Override
    public void run() {
        timeNow = System.nanoTime();
        timeSpentTask1 = timeNow - timeSpentTask1;
        timeSpentTotal = timeSpentTotal + timeSpentTask1;

        for (int i = 0; i < 520000; i++) {
            System.out.println("Hello |1| - " + timeNow + "--" + i);
        }

    }

    public long getTimeSpent() {
        return timeSpentTotal;
    }

}
