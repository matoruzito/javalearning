/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package javalearning.multithreading;

/**
 *
 * @author matias
 */
public class Task2 implements Runnable {

    private long timeSpentTask2 = 0;
    private long timeNow = 0;
    private long timeSpentTotal = 0;

    public Task2() {

    }

    @Override
    public void run() {
        timeNow = System.nanoTime();
        timeSpentTask2 = timeNow - timeSpentTask2;
        timeSpentTotal = timeSpentTotal + timeSpentTask2;

        for (int i = 0; i < 52000; i++) {
            System.out.println("Hello |2|  - " + timeNow + "--" + i);
        }
    }
    
    
    public long getTimeSpent(){
        return timeSpentTotal;
    }
    
}
