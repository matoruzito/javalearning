package javalearning.multithreading;

public class Task3 implements Runnable {

    private long timeSpentTask3 = 0;
    private long timeNow = 0;
    private long timeSpentTotal = 0;

    public Task3() {

    }

    @Override
    public void run() {
        timeNow = System.nanoTime();
        timeSpentTask3 = timeNow - timeSpentTask3;
        timeSpentTotal = timeSpentTotal + timeSpentTask3;

        for (int i = 0; i < 52000; i++) {
            System.out.println("Hello |3|  - " + timeNow + "--" + i);

        }
    }

    public long getTimeSpent() {
        return timeSpentTotal;
    }
}
