Binary IO: 
    |
    |--> Binary Input Output does not require conversions. The exact value in the memory is copied into the file. 
    |--> When reading a byte using binary I/O, one byte value is read from the input.
    |
    |--> When reading text-editor-created file or text-output program, it is better to use text input. 
    |--> When reading java-binary output program, it is better to use binary I/O.
    |
    |--> Binary I/O is more efficient than text I/O, because binary I/O does not require encoding and decoding.
    |       Binary files are independent of the encoding scheme on the host machine and thus are portable. 
    |--> Java class files are binary for being readable by a JVM on any machine.
    |
    |
    |--> Binary I/O Classes
    |       |
    |       |--> abstract InputStream (Reading binary data)
    |       |
    |       |   METHODS OF InputStream:
    |       |   
    |       |   +read(): int
    |       |   +read(b: byte[]): int
    |       |   +read(b: byte[], off: int,
    |       |    len: int): int
    |       |   +available(): int
    |       |   +close(): void
    |       |   +skip(n: long): long
    |       |   +markSupported(): boolean
    |       |   +mark(readlimit: int): void
    |       |   +reset(): void

    |       |--> abstract OutputStream (Writting binary data)
    |       |
    |       |   METHODS OF OutputStream:
    |       |
    |       |   +write(int b): void
    |       |   +write(b: byte[], off: int,
    |       |   len: int): void
    |       |   +write(b: byte[]): void
    |       |   +close(): void
    |       |   +flush(): void
    | 
    |--> When a stream is no longer needed, always close it using close() !        
    |      
    |--> Unicode Character consist of two bytes.   
    |       
    |
    |--> DataOutputStream :
    |       | -> DataOutputStream Class enables you to write primitive data type values and strings into an output stream.
    |       |       Examples of using DataOutputStream to write inside a file: 
    |       |       
    |       |       DataOutputStream output = new DataOutputStream(new FileOutputStream("filename.dat")); //Instancing
    |       |       output.writeUTF("Doe");
    |       |       output.writeDouble(55.6);
    |       |       
    |       |       output.close();
    |       |
    |       |