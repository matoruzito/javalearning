Abstract Classes
    |
    |--> Abstract Methods
    |           |
    |           |--> Methods from superclass that only can be implemented instancing some child class are known
    |           |    as ABSTRACT METHODS. They are denoted using "abstract" modifier.
    |           |--> After you define some abstract method, the class from they are becomes an ABSTRACT CLASS.
    |
    |--> Abstract classes are like regular classes, but they can not be instanced.
    |
    |--> The constructor of an abstract class has modifier "protected" because it can only be used by subclasses.
    |
    |--> If a subclass of an abstract superclass does not implement all the abstract methods, the subclass must be
    |    defined as abstract. 
    |
    |--> Abstract methods are non-static. 
    |
