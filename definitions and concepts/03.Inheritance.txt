Inheritance:
    |
    |--> Object-oriented programming allows you to define new classes from existing classes. This is "Inheritance".
    |     |--> Useful feature for reusing code.
    |     
    |--> It enables you to define a general class "Superclass" and later extend it to more specialized classes: "Subclasses".
    |     |--> Different classes may have some common properties and behaviors, which can be generalized in a class that can be
    |            shared by other classes. The specialized classes inherit the properties and methods from the Superclass.
    |
    |-----Superclass: Also called "Parent class" or "Base class".
    |-----Subclass: Also called "Child class", "extended class" or "derived class".
    |
    |----- public class [SubClassName] extends [SuperClassName].
    |      |--> It enables "SubClassName" to inherit methods and properties from "SuperClassName".
    |
    |----- "super" keyword -> For calling methods or constructors of a superclass.
    |        |--> Calling superclass constructors: They can only be invoked from the constructors of the
    |        |        subclasses, using the keyword "super": "super()", or "super(params)" 
    |        |--> Calling superclass methods: "super.method(params)".
    |
    |--> Constructing an instance of a class invokes the constructors of all the superclasses along the 
    |        inheritance chain. 
    |
    |-> Overriding: Provide a new implementation for a method in the subclass.
    |       |-> @Override -> This annotation denotes that the annotated method is required to override 
    |                           a method in the superclass. The compiler will report an error if the method    
    |                           is not overriding any superclass's method. 
    |-> Overloading: Define multiple methods with the same name but different signatures.
    |
    |
    |--- Every class in Java is descended from the java.lang.Object class. 
    |
    |
    

Recomendations: -> Consider always making a no-args constructor of every superClass.

