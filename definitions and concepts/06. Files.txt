File Class: 
    | -> File Class does not contain the methods for reading and writing file contents.
    |--> new File("C:\\libros") creates a File object for the directory C:\libros.
    |--> new file ("C:\\libros\filetest.dat") creates a File object for the file C:\libros\filetest.dat.

Attention! Backslash is a special character in java, in a string literal it should be written as \\ 

USEFUL METHODS OF JAVA.IO.FILE 

+File(pathname: String)
+File(parent: String, child: String)
+File(parent: File, child: String)
+exists(): boolean
+canRead(): boolean
+canWrite(): boolean
+isDirectory(): boolean
+isFile(): boolean
+isAbsolute(): boolean
+isHidden(): boolean
+getAbsolutePath(): String
+getCanonicalPath(): String
+getName(): String
+getPath(): String
+getParent(): String
+lastModified(): long
+length(): long
+listFile(): File[]
+delete(): boolean
+renameTo(dest: File): boolean
+mkdir(): boolean
+mkdirs(): boolean


#### Reading and writting data from and to files

READ DATA =>    Scanner
WRITE DATA =>   PrintWriter 


PrintWriter methods

+PrintWriter(filename: String)    ------> Constructor already having an instance of File.
+print(s: String): void           ------> Constructor having the specified file-name string.
+PrintWriter(file: File)
+print(c: char): void
+print(cArray: char[]): void
+print(i: int): void
+print(l: long): void
+print(f: float): void
+print(d: double): void
+print(b: boolean): void
Also contains the overloaded
 println methods.
Also contains the overloaded
 printf methods